package com.mhs.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mhs.entity.Knowledge;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lyl
 * @since 2023-04-06
 */
public interface KnowledgeMapper extends BaseMapper<Knowledge> {

    @Select("select * from knowledge")
    public List<Knowledge> SelectAll();
}
