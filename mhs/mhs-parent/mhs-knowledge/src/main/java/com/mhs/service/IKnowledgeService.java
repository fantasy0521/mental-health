package com.mhs.service;

import com.mhs.entity.Knowledge;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lyl
 * @since 2023-04-06
 */
public interface IKnowledgeService extends IService<Knowledge> {


   public List<Knowledge> queryAll();
}
