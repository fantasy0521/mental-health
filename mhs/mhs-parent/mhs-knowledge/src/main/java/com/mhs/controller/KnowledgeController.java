package com.mhs.controller;


import com.mhs.entity.Knowledge;
import com.mhs.service.IKnowledgeService;
import com.mhs.vo.Result;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author lyl
 * @since 2023-04-06
 */
@RestController
@RequestMapping("/mentalhealth/knowledge")
public class KnowledgeController {

    @Resource
    private IKnowledgeService service;

    @CrossOrigin(origins = {"*"})
    @GetMapping("queryKnowledge")
    public Result queryKnowledge() {
        List<Knowledge> list = service.queryAll();
        if (list == null || list.isEmpty()) {
            return new Result(0, "查无数据", null);
        }
        return new Result(1, "查询成功", list);
    }

}

