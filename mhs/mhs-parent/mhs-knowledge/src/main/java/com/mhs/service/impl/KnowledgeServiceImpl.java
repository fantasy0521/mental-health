package com.mhs.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mhs.entity.Knowledge;
import com.mhs.mapper.KnowledgeMapper;
import com.mhs.service.IKnowledgeService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author lyl
 * @since 2023-04-06
 */
@Service
public class KnowledgeServiceImpl extends ServiceImpl<KnowledgeMapper, Knowledge> implements IKnowledgeService {
    @Resource
    private KnowledgeMapper mapper;

    /**
     * 查询所有小知识
     *
     * @return
     */
    @Override
    public List<Knowledge> queryAll() {
        List<Knowledge> knowledges = mapper.SelectAll();
        if (knowledges == null || knowledges.isEmpty()) {
            return null;
        }
        return knowledges;
    }
}
