import com.mhs.KnowledgeApp;
import com.mhs.entity.Knowledge;
import com.mhs.mapper.KnowledgeMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;

@SpringBootTest(classes = KnowledgeApp.class)
public class KnowledgeServiceImplTest {

    @Resource
    private KnowledgeMapper mapper;

    @Test
    public void test(){
        List<Knowledge> knowledges = mapper.SelectAll();
        System.out.println(knowledges);
    }

}
