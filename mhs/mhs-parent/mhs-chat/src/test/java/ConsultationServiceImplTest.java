import com.mhs.ChatApp;
import com.mhs.entity.Consultation;
import com.mhs.mapper.ConsultationMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;

//@SpringBootTest(classes = ChatApp.class)
public class ConsultationServiceImplTest {

    @Resource
    private ConsultationMapper mapper;

    @Test
    public void test(){
        List<Consultation> consultations = mapper.SelectAll();
        System.out.println(consultations);
    }
}
