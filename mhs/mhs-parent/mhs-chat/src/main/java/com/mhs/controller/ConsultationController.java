package com.mhs.controller;


import com.mhs.entity.Consultation;
import com.mhs.service.IConsultationService;
import com.mhs.vo.Result;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lyl
 * @since 2023-04-18
 */
@RestController
@RequestMapping("/mhs/consultation")
public class ConsultationController {

    @Resource
    private IConsultationService service;

//    @CrossOrigin(origins = {"*"})
    @GetMapping("queryAll")
    public Result queryAll(){
        List<Consultation> list = service.queryAll();
        if (list == null || list.isEmpty()){
            return new Result(0,"查询失败",null);
        }
        return new Result(1,"查询成功",list);
    }


    /**
     * 审核咨询师
     * @return
     */
    @GetMapping("queryByPage")
    @PreAuthorize("@fse.hasAuthority('admin')")
    public Result queryByPage(@RequestParam(required = false,name = "content") String content,
                              @RequestParam(defaultValue = "1") Integer pageNum,
                              @RequestParam(defaultValue = "10") Integer pageSize){
        return service.queryByPage(content,pageNum,pageSize);
    }

    @PostMapping("updateStatus")
    @PreAuthorize("@fse.hasAuthority('admin')")
    public Result updateStatus(@RequestBody Consultation consultation){
        return service.updateStatusById(consultation);
    }
    
    @GetMapping("deleteByCid")
    @PreAuthorize("@fse.hasAuthority('admin')")
    public Result deleteByCid(Integer cid){
        service.removeById(cid);
        return Result.success("删除成功");
    }
    

//    @CrossOrigin(origins = {"*"})
    @GetMapping("queryId")
    public Result queryId(int id){
        Consultation consultation = service.queryId(id);
        if (consultation == null){
            return new Result(0,"查询失败",null);
        }
        return new Result(1,"查询成功",consultation);
    }

    @PostMapping("AddConsultation")
    public Result AddConsultation(@RequestBody Consultation consultation){
        int num = service.addConsultation(consultation);
        if (num == 0){
            return new Result(0,"添加失败",null);
        }
        return new Result(1,"添加成功",num);
    }

    @GetMapping("queryByUid")
    public Result queryByUid(int id){
        Consultation consultation = service.queryByUid(id);
        if (consultation == null){
            return new Result(0,"查询失败",null);
        }
        return new Result(1,"查询成功",consultation);
    }


}

