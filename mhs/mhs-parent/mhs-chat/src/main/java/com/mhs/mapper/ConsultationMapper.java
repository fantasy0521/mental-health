package com.mhs.mapper;

import com.mhs.entity.Consultation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mhs.entity.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lyl
 * @since 2023-04-18
 */
public interface ConsultationMapper extends BaseMapper<Consultation> {


    @Select("select * from consultation where state = 1")
    @Results(
            value = {
                    @Result(column = "cid", property = "cid", id = true),
//                    一对多关联查询，是用主表id查询从表
//                    一对一关联查询，是用外键关联字段来查询另一个表
                    @Result(column = "id", property = "user", one = @One(
                            select = "SelectByid"
                    ))
            }
    )
    public List<Consultation> SelectAll();


    @Select("select * from user where id = #{id}")
    public User SelectByid(int id);


    @Select("select * from consultation where cid = #{id}")
    @Results(
            value = {
                    @Result(column = "cid", property = "cid", id = true),
//                    一对多关联查询，是用主表id查询从表
//                    一对一关联查询，是用外键关联字段来查询另一个表
                    @Result(column = "id", property = "user", one = @One(
                            select = "SelectByid"
                    ))
            }
    )
    public Consultation SelectById(int id);

    @Insert("insert into values(null,#{introduce},#{territory},#{number},#{year},#{address},#{id},0)")
    public int Add(@Param("introduce") String introduce,
                   @Param("territory") String territory,
                   @Param("number") int number,
                   @Param("year") String year,
                   @Param("address") String address,
                   @Param("id") int id);

    @Select("select * from consultation where id = #{id}")
    public Consultation SelectByUid(int id);


    void updateByUserId(Integer userId);
}
