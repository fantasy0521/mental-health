package com.mhs.service.impl;

import com.mhs.entity.Message;
import com.mhs.mapper.IMessageMapper;
import com.mhs.service.IMessageService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lyl
 * @since 2023-04-15
 */
@Service
public class MessageServiceImpl  implements IMessageService {

    @Resource
    private IMessageMapper mapper;

    @Override
    public List<Message> querySingle(int uid, int toUid) {
        List<Message> messages = mapper.SelectOneToOne(uid, toUid);
        if (messages == null || messages.isEmpty()){
            return null;
        }
        return messages;
    }
}
