package com.mhs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mhs.dto.ChatDTO;
import com.mhs.entity.Chat;
import com.mhs.entity.User;
import org.apache.ibatis.annotations.*;

import java.util.List;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;

public interface IChatMapper extends BaseMapper<Chat> {
    @Select("select * from chat where consultant_id = #{id} or consumer_id = #{id}")
    @Results({

            @Result(column = "consultant_id", property = "consulant", one = @One(select = "getUserByid")),
            @Result(column = "consumer_id", property = "consumer", one = @One(select = "getUserByid"))
    })
    public List<ChatDTO> getChats(long id);



    @Select("select id, nick_name ,avatar from user where id =#{id}")
    User getUserByid(long id);



    @Insert("insert into chat values(null , #{id}, #{id}, #{time}) ")
    public int Add(@Param("id") int id,
                       @Param("time") String time);
}
