package com.mhs.service;

import com.mhs.entity.Message;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lyl
 * @since 2023-04-15
 */
public interface IMessageService  {

    public List<Message> querySingle(int uid, int toUid);
}
