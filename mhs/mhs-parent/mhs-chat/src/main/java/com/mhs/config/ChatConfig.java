package com.mhs.config;

import com.mhs.component.WebSocketServer;
import com.mhs.mapper.IChatMapper;
import com.mhs.mapper.IMessageMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

import javax.annotation.Resource;

@Configuration
public class ChatConfig {
    @Resource
    WebSocketServer webSocketServer;

    @Resource
    IMessageMapper messageMapper;
    @Resource
    IChatMapper chatMapper;

    @Resource
    RedisTemplate redisTemplate;
    @Bean
    public ServerEndpointExporter serverEndpointExporter(){
        webSocketServer.setMessageMapper(messageMapper);
        webSocketServer.setChatMapper(chatMapper);
        webSocketServer.setRedisTemplate(redisTemplate);
        return new ServerEndpointExporter();
    }

}
