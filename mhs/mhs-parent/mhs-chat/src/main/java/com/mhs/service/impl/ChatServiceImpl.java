package com.mhs.service.impl;

import com.mhs.dto.ChatDTO;
import com.mhs.entity.Chat;
import com.mhs.mapper.IChatMapper;
import com.mhs.mapper.IChatMapper;
import com.mhs.mapper.IMessageMapper;
import com.mhs.service.IChatService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ChatServiceImpl implements IChatService {
    @Resource
    private IChatMapper mapper;


    @Override
    public List<ChatDTO> getChats(long id) {
        List<ChatDTO> chats = mapper.getChats(id);
        return chats;
    }
    @Override
    public int Add(int id, String time) {
        int num = mapper.Add(id, time);
        return num;
    }
}
