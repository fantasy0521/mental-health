package com.mhs.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mhs.entity.Consultation;
import com.mhs.mapper.ConsultationMapper;
import com.mhs.service.IConsultationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mhs.vo.Result;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lyl
 * @since 2023-04-18
 */
@Service
public class ConsultationServiceImpl extends ServiceImpl<ConsultationMapper, Consultation> implements IConsultationService {

    @Resource
    private ConsultationMapper mapper;

    @Override
    public List<Consultation> queryAll() {
        List<Consultation> consultations = mapper.SelectAll();
        if ( consultations == null || consultations.isEmpty() ){
            return null;
        }
        return consultations;
    }

    @Override
    public Consultation queryId(int id) {
        Consultation consultation = mapper.SelectById(id);
        if (consultation == null){
            return null;
        }
        return consultation;
    }

    @Override
    public int AddConsultation(String introduce, String territory, int number, String year, String address, int id) {
        int num = mapper.Add(introduce, territory, number, year, address, id);
        return num;
    }

    @Override
    public int addConsultation(Consultation consultation) {
        consultation.setState(0);
        boolean save = this.save(consultation);
        if(save){
            return 1;
        }
        return 0;
    }

    @Override
    public Consultation queryByUid(int id) {
        Consultation consultation = mapper.SelectByUid(id);
        if (consultation == null){
            return null;
        }
        return consultation;
    }

    @Override
    public Result queryByPage(String content, Integer pageNum, Integer pageSize) {
        IPage<Consultation> page = new Page<>();
        page.setPages(pageNum);
        page.setSize(pageSize);
        LambdaQueryWrapper<Consultation> queryWrapper = new LambdaQueryWrapper<>();;
        if (content == null || content.isEmpty()) {
            page = this.page(page);
            return Result.success(page);
        }
        queryWrapper.like(Consultation::getId,content);
        page = this.page(page, queryWrapper);
        return Result.success(page);
    }

    @Override
    public Result updateStatusById(Consultation consultation) {
        this.updateById(consultation);
        //提升用户权限等级
        if (consultation.getState() == 1){
            //todo 远程调用user服务
            //这里直接修改 sys_user_id 表
            mapper.updateByUserId(consultation.getId());
        }
        return Result.success("修改审核状态成功");
    }
}
