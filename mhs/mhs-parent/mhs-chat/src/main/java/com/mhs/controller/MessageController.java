package com.mhs.controller;

import com.mhs.entity.Message;
import com.mhs.service.IMessageService;
import com.mhs.vo.Result;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;
import java.util.List;


@RestController
@RequestMapping("/mhs/message")
public class MessageController {
    @Resource
    private IMessageService service;

    @GetMapping("querySingle")
    public Result querySingle(@RequestParam("uid") int uid,
                              @RequestParam("toUid") int toUid){
        String a = "asdf";
        List<Message> list = service.querySingle(uid, toUid);
        if (list == null || list.isEmpty()){
            return new Result(0,"查询失败",null);
        }
        return new Result(1,"查询成功",list);
    }
}
