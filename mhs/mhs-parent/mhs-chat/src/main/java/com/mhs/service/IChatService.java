package com.mhs.service;

import com.mhs.dto.ChatDTO;
import com.mhs.entity.Chat;

import java.util.List;

import com.mhs.mapper.IChatMapper;

public interface IChatService {

  List<ChatDTO> getChats(long id);

    public int Add(int id, String time);
}
