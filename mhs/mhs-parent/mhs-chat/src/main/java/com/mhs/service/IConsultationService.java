package com.mhs.service;

import com.mhs.entity.Consultation;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mhs.vo.Result;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lyl
 * @since 2023-04-18
 */
public interface IConsultationService extends IService<Consultation> {


    public List<Consultation> queryAll();

    public Consultation queryId(int id);

    public int AddConsultation(String introduce,String territory,
                                int number, String year,
                               String address, int id);

    int addConsultation(Consultation consultation);

    public Consultation queryByUid(int id);

    Result queryByPage(String content, Integer pageNum, Integer pageSize);

    Result updateStatusById(Consultation consultation);
}
