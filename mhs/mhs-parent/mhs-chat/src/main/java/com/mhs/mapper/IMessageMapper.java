package com.mhs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mhs.entity.Message;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.time.LocalDateTime;
import java.util.List;

public interface IMessageMapper extends BaseMapper<Message> {

    @Insert("insert into message (content,from_uid,to_uid,creat_time) values(#{content},#{from},#{to},#{creat})")
    public int insertMessage(@Param("content")String content, @Param("from") long fromUid, @Param("to") long toUid, @Param("creat") LocalDateTime creat);

    //@Select("select * from message where from_uid = #{uid} and to_uid = #{toUid} order by creat_time union select * from message where from_uid = #{toUid} and to_uid = #{uid}")
//    @Select("select * from message where from_uid = #{uid} or  from_uid =  #{toUid} and to_uid = #{toUid} or  to_uid = #{uid} order by creat_time limit 0,50")
    @Select("select * from message where (from_uid = #{uid} and  to_uid =  #{toUid}) or (from_uid = #{toUid} and  to_uid = #{uid}) order by creat_time limit 0,50")
    public List<Message> SelectOneToOne(@Param("uid") int uid,
                                        @Param("toUid") int toUid);}
