package com.mhs.controller;

import com.mhs.component.WebSocketServer;
import com.mhs.dto.ChatDTO;
import com.mhs.entity.Chat;
import com.mhs.service.IChatService;
import com.mhs.service.impl.ChatServiceImpl;
import com.mhs.vo.Result;
import com.mhs.service.IChatService;
import com.mhs.vo.Result;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@RestController
@MapperScan("com.mhs.mapper")
@RequestMapping("mhs/room")
public class ChatController {
    @Resource
    ChatServiceImpl  chatService;
//
    @RequestMapping(value = "getChats/{id}",method = RequestMethod.GET)
    public Result getChats(@PathVariable("id") long id){
      //  System.out.println(Long.getLong(id));
        List<ChatDTO> chats = chatService.getChats(id);
        return new Result(200,"查询成功",chats);
    }

    @Resource
    private IChatService service;

    @GetMapping("Add")
    public Result Add(int id){
        LocalDateTime dateTime = LocalDateTime.now(); // get the current date and time
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        System.out.println(dateTime.format(formatter));
        String time = dateTime.format(formatter);
        int num = service.Add(id, time);
        if (num == 0){
            return new Result(0,"添加失败",null);
        }
        return new Result(1,"添加成功",num);

    }
}
