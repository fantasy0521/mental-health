package com.mhs;



import com.mhs.component.WebSocketServer;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

import javax.annotation.Resource;

@SpringBootApplication(exclude= {SecurityAutoConfiguration.class })
//@SpringBootApplication
@MapperScan("com.mhs.mapper")
public class ChatApp {

    public static void main(String[] args) {
        SpringApplication.run(ChatApp.class,args);
    }




}
