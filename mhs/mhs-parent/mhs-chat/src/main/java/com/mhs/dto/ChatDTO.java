package com.mhs.dto;

import com.mhs.entity.Chat;
import com.mhs.entity.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChatDTO extends Chat {
    public User consulant;
    public User consumer;
}
