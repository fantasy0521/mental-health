import com.mhs.EssayApp;
import com.mhs.entity.Review;
import com.mhs.service.IReviewService;
import com.mhs.service.IReplyService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@SpringBootTest(classes = EssayApp.class)
public class CommentServiceImplTest {
    @Resource
    private IReviewService service;
    @Resource
    private IReplyService replyService;

    @Test
    public void test(){
        List<Review> comments = service.queryEid(1);
        for (Review comment : comments) {
            System.out.println(comment);
            int count = replyService.countReply(comment.getCid());
            comment.setCountReply(count);
        }
        System.out.println(comments);
    }

    @Test
    public void test1(){
        LocalDateTime dateTime = LocalDateTime.now(); // get the current date and time
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        System.out.println(dateTime.format(formatter));
        String time = dateTime.format(formatter);
        int num = service.addComment("一日之计在于晨", 1, 15, time,null);
        System.out.println(num);
    }
}
