import com.mhs.EssayApp;
import com.mhs.mapper.ReadMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest(classes = EssayApp.class)
public class ReadServiceImplTest {

    @Resource
    private ReadMapper mapper;

    @Test
    public void test(){
        int alter = mapper.Alter(1, 1, 15);
        System.out.println(alter);
    }

    @Test
    public void test1(){
        int num = mapper.Increase(15, 9, 0);
        System.out.println(num);
    }
}
