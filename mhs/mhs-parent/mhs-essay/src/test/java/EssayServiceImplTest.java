import com.mhs.EssayApp;
import com.mhs.dto.EssayDto;
import com.mhs.entity.Essay;
import com.mhs.mapper.EssayMapper;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;

@SpringBootTest(classes = EssayApp.class)
public class EssayServiceImplTest {

    @Resource
    private EssayMapper mapper;

    @Test
    public void test(){
        List<Essay> essays = mapper.SelectAll();
        System.out.println(essays);
    }

    @Test
    public void test1(){
        Essay essay = mapper.selectById(1);
        System.out.println(essay);
    }


    @Test
    public void test123(){
        List<EssayDto> essayDtos = mapper.SelectAllByUserId(15);
        System.out.println(essayDtos);
    }
}
