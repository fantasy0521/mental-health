import com.mhs.EssayApp;
import com.mhs.entity.Reply;
import com.mhs.service.IReplyService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@SpringBootTest(classes = EssayApp.class)
public class ReplyServiceImplTest {

    @Resource
    private IReplyService service;

    @Test
    public void test(){
        List<Reply> replies = service.queryCid(1);
        System.out.println(replies);
    }

    @Test
    public void test1(){
        List<Reply> replies = service.queryToRid(1);
        System.out.println(replies);
    }

    @Test
    public void test2(){
        LocalDateTime dateTime = LocalDateTime.now(); // get the current date and time
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        System.out.println(dateTime.format(formatter));
        String time = dateTime.format(formatter);
        int num = service.addReply("返回大家看法", 15, 1, time,null);
        System.out.println(num);
    }

    @Test
    public void test3(){
        LocalDateTime dateTime = LocalDateTime.now(); // get the current date and time
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        System.out.println(dateTime.format(formatter));
        String time = dateTime.format(formatter);
        int num = service.addReplys("说说自己的意见", 15, 1, time,null);
        System.out.println(num);
    }
}
