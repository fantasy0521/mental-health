package com.mhs.mapper;

import com.mhs.entity.Reply;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mhs.entity.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lyl
 * @since 2023-04-11
 */
public interface ReplyMapper extends BaseMapper<Reply> {


    @Select("select * from reply where cid = #{id} order by create_time desc")
    @Results(
            value = {
                    @Result(column = "rid", property = "rid", id = true),
//                    一对多关联查询，是用主表id查询从表
//                    一对一关联查询，是用外键关联字段来查询另一个表
                    @Result(column = "id", property = "user", one = @One(
                            select = "SelectByid"
                    ))
            }
    )
    public List<Reply> SelectByCid(int id);

    @Select("select * from reply where to_rid = #{id} order by create_time desc")
    @Results(
            value = {
                    @Result(column = "rid", property = "rid", id = true),
//                    一对多关联查询，是用主表id查询从表
//                    一对一关联查询，是用外键关联字段来查询另一个表
                    @Result(column = "id", property = "user", one = @One(
                            select = "SelectByid"
                    ))
            }
    )
    public List<Reply> SelectByToRid(int id);

    @Select("select count(*) from reply where cid = #{id}")
    public int countReply(int id);

    @Select("select * from user where id = #{id}")
    public User SelectByid(int id);

    @Insert("insert into reply values(null,#{content},#{id},null,#{cid},#{time},#{image})")
    public int addReply(@Param("content") String content,
                          @Param("id") int id,
                          @Param("cid") int cid,
                          @Param("time") String time,
                          @Param("image") String image);

    @Insert("insert into reply values(null,#{content},#{id},#{toRid},null,#{time},#{image})")
    public int addReplys(@Param("content") String content,
                        @Param("id") int id,
                        @Param("toRid") int toRid,
                        @Param("time") String time,
                        @Param("image") String image);
}
