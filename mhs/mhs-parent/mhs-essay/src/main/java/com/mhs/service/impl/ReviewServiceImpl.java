package com.mhs.service.impl;

import com.mhs.entity.Review;
import com.mhs.mapper.ReviewMapper;
import com.mhs.service.IReviewService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lyl
 * @since 2023-04-11
 */
@Service
public class ReviewServiceImpl extends ServiceImpl<ReviewMapper, Review> implements IReviewService {

    @Resource
    private ReviewMapper mapper;

    @Override
    public List<Review> queryEid(int id) {
        List<Review> comments = mapper.SelectByEid(id);
        if (comments == null || comments.isEmpty()){
            return null;
        }
        return comments;
    }

    @Override
    public int addComment(String content, int eid, int uid, String time, String image) {
        int num = mapper.addComment(content, eid, uid, time,image);
        return num;
    }
}
