package com.mhs.service;

import com.mhs.dto.EssayDto;
import com.mhs.entity.Essay;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mhs.vo.Result;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lyl
 * @since 2023-04-09
 */
public interface IEssayService extends IService<Essay> {


    public List<Essay> queryAll();

    public Essay queryId(int id);

    public List<EssayDto> queryAllIsCollect(int id);


    Result deletEssay(Integer eid);

    Result getAllEssay();
}
