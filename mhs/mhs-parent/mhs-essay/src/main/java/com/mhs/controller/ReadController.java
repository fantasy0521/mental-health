package com.mhs.controller;


import com.mhs.entity.Essay;
import com.mhs.entity.Read;
import com.mhs.service.IReadService;
import com.mhs.utils.BaseContext;
import com.mhs.vo.Result;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lyl
 * @since 2023-04-20
 */
@RestController
@RequestMapping("/mhs/read")
public class ReadController {

    @Resource
    private IReadService service;

//    @CrossOrigin(origins = {"*"})
    @GetMapping("changeState")
    public Result changeState(@RequestParam("state") int state,
                              @RequestParam("id") int id){
        Long currentId = BaseContext.getCurrentId();
        int uid = currentId.intValue();
        int num = service.ChangeState(state, id, uid);
        if (num == 0){
            return new Result(0,"更改失败",null);
        }
        return new Result(1,"更改成功",num);

    }

//    @CrossOrigin(origins = {"*"})
    @GetMapping("queryIsCollect")
    public Result queryIsCollect(){
        Long currentId = BaseContext.getCurrentId();
        int uid = currentId.intValue();
        List<Read> list = service.queryIsCollect(uid);
        if (list == null || list.isEmpty()){
            return new Result(0,"查无数据", null);
        }
        return new Result(1,"查询成功",list);
    }

//    @CrossOrigin(origins = {"*"})
    @GetMapping("queryState")
    public Result queryState(@RequestParam("id") int id){
        Long currentId = BaseContext.getCurrentId();
        int uid = currentId.intValue();
        Read read = service.queryState(id, uid);
        if (read == null){
            return new Result(0,"查无数据", null);
        }
        return new Result(1,"查询成功",read);
    }

//    @CrossOrigin(origins = {"*"})
    @GetMapping("AddRead")
    public Result AddRead(@RequestParam("id") int id,
                          @RequestParam("state") int state){
        Long currentId = BaseContext.getCurrentId();
        int uid = currentId.intValue();
        int num = service.AddRead(uid, id, state);
        if (num == 0){
            return new Result(0,"添加失败", null);
        }
        return new Result(1,"添加成功",num);
    }

    @GetMapping("AlterState")
    public Result AlterState(@RequestParam("state") int state,
                              @RequestParam("id") int id){
        Long currentId = BaseContext.getCurrentId();
        int uid = currentId.intValue();
        int num = service.AlterState(state, id, uid);
        if (num == 0){
            return new Result(0,"更改失败",null);
        }
        return new Result(1,"更改成功",num);

    }

    @GetMapping("IncreaseRead")
    public Result IncreaseRead(@RequestParam("id") int id,
                          @RequestParam("state") int state){
        Long currentId = BaseContext.getCurrentId();
        int uid = currentId.intValue();
        int num = service.IncreaseRead(uid, id, state);
        if (num == 0){
            return new Result(0,"添加失败", null);
        }
        return new Result(1,"添加成功",num);
    }

    @GetMapping("FindState")
    public Result FindState(@RequestParam("id") int id){
        Long currentId = BaseContext.getCurrentId();
        int uid = currentId.intValue();
        Read read = service.FindState(id, uid);
        if (read == null){
            return new Result(0,"查无数据", null);
        }
        return new Result(1,"查询成功",read);
    }
}

