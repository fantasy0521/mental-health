package com.mhs.mapper;

import com.mhs.entity.Review;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mhs.entity.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lyl
 * @since 2023-04-11
 */
public interface ReviewMapper extends BaseMapper<Review> {

    @Select("select * from review where eid = #{id} order by create_time desc")
    @Results(
            value = {
                    @Result(column = "cid", property = "cid", id = true),
//                    一对多关联查询，是用主表id查询从表
//                    一对一关联查询，是用外键关联字段来查询另一个表
                    @Result(column = "id", property = "user", one = @One(
                            select = "SelectByid"
                    ))
            }
    )
    public List<Review> SelectByEid(int id);


    @Select("select * from user where id = #{id}")
    public User SelectByid(int id);

    @Insert("insert into review values(null,#{content},#{eid},#{id},null,#{time},#{image})")
    public int addComment(@Param("content") String content,
                          @Param("eid") int eid,
                          @Param("id") int uid,
                          @Param("time") String time,
                          @Param("image") String image);

}
