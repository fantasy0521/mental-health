package com.mhs.service;

import com.mhs.entity.Essay;
import com.mhs.entity.Read;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lyl
 * @since 2023-04-20
 */
public interface IReadService extends IService<Read> {

    public int ChangeState(int state, int id, int uid);

    public List<Read> queryIsCollect(int id);

    public Read queryState(int id, int uid);

    public int AddRead(int uid,int id,int state);

    public int AlterState(int state, int id, int uid);

    public int IncreaseRead(int uid, int id, int state);

    public Read FindState(int id, int uid);
}
