package com.mhs.controller;


import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.mhs.dto.EssayDto;
import com.mhs.entity.Essay;
import com.mhs.service.IEssayService;
import com.mhs.utils.BaseContext;
import com.mhs.vo.Result;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lyl
 * @since 2023-04-09
 */
@RestController
@RequestMapping("/mhs/essay")
public class EssayController {

    @Resource
    private IEssayService service;

//    @CrossOrigin(origins = {"*"})
    @GetMapping("queryAll")
    public Result queryAll(){
        List<Essay> list = service.queryAll();
        if (list == null || list.isEmpty()){
            return new Result(0,"查无数据", null);
        }
        return new Result(1,"查询成功",list);
    }

//    @CrossOrigin(origins = {"*"})
    @GetMapping("queryId")
    public Result queryId(int id){
//        Long currentId = BaseContext.getCurrentId();
        Essay essay = service.queryId(id);
        if (essay == null){
            return new Result(0,"查无数据", null);
        }
        return new Result(1,"查询成功",essay);
    }

//    @CrossOrigin(origins = {"*"})
    @GetMapping("queryAllIsCollect")
    public Result queryAllIsCollect(){
        Long currentId = BaseContext.getCurrentId();
        int id = currentId.intValue();
        System.out.println(id);
        List<EssayDto> list = service.queryAllIsCollect(id);
        if (list == null || list.isEmpty()){
            return new Result(0,"查无数据", null);
        }
        return new Result(1,"查询成功",list);
    }

    /**
     * 添加文章
     * @param essay
     * @return
     */
    @PostMapping("addEssay")
    @PreAuthorize("@fse.hasAuthority('admin')")
    public Result addEssay(@RequestBody Essay essay){
        service.save(essay);
        return Result.success("保存成功");
    }
    
    
    @DeleteMapping("deletEssay")
    @PreAuthorize("@fse.hasAuthority('admin')")
    public Result deletEssay(@RequestParam("eid") Integer eid){
        return service.deletEssay(eid);
    }
    
    @GetMapping("getAllEssay")
    public Result getAllEssay(){
        return service.getAllEssay();
    }
    
    
    @PutMapping("updateEssay")
    @PreAuthorize("@fse.hasAuthority('admin')")
    public Result updateEssay(@RequestBody Essay essay){
        service.updateById(essay);
        return Result.success("修改文章成功");
    }
    
    
}

