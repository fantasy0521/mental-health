package com.mhs;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication()
@MapperScan("com.mhs.mapper")
public class EssayApp {

    public static void main(String[] args) {
        SpringApplication.run(EssayApp.class,args);
    }
}
