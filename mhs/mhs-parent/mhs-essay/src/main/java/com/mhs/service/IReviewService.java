package com.mhs.service;

import com.mhs.entity.Review;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lyl
 * @since 2023-04-11
 */
public interface IReviewService extends IService<Review> {

    public List<Review> queryEid(int id);

    public int addComment(String content,int eid,int uid,String time,String image);
}
