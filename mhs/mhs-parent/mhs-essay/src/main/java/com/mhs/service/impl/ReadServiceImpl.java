package com.mhs.service.impl;

import com.mhs.entity.Essay;
import com.mhs.entity.Read;
import com.mhs.mapper.ReadMapper;
import com.mhs.service.IReadService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lyl
 * @since 2023-04-20
 */
@Service
public class ReadServiceImpl extends ServiceImpl<ReadMapper, Read> implements IReadService {

    @Resource
    private ReadMapper mapper;

    @Override
    public int ChangeState(int state, int id, int uid) {
        int num = mapper.Change(state, id, uid);
        return num;
    }

    @Override
    public List<Read> queryIsCollect(int id) {
        List<Read> essays = mapper.queryState(id);
        if (essays == null || essays.isEmpty()){
            return null;
        }
        return essays;
    }

    @Override
    public Read queryState(int id, int uid) {
        Read read = mapper.SelectIsCollect(id, uid);
        if (read == null){
            return null;
        }
        return read;
    }

    @Override
    public int AddRead(int uid, int id, int state) {
        int num = mapper.Add(uid, id, state);
        return num;
    }

    @Override
    public int AlterState(int state, int id, int uid) {
        int num = mapper.Alter(state, id, uid);
        return num;
    }

    @Override
    public int IncreaseRead(int uid, int id, int state) {
        int num = mapper.Increase(uid, id, state);
        return num;
    }

    @Override
    public Read FindState(int id, int uid) {
        Read read = mapper.FindState(id, uid);
        if (read == null){
            return null;
        }
        return read;
    }
}
