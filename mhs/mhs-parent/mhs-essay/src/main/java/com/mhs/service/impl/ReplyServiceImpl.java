package com.mhs.service.impl;

import com.mhs.entity.Reply;
import com.mhs.mapper.ReplyMapper;
import com.mhs.service.IReplyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lyl
 * @since 2023-04-11
 */
@Service
public class ReplyServiceImpl extends ServiceImpl<ReplyMapper, Reply> implements IReplyService {

    @Resource
    private ReplyMapper mapper;

    @Override
    public List<Reply> queryCid(int id) {
        List<Reply> replies = mapper.SelectByCid(id);
        if (replies == null || replies.isEmpty()){
            return null;
        }
        return replies;
    }

    @Override
    public int countReply(int id) {
        int count = mapper.countReply(id);
        return count;
    }

    @Override
    public List<Reply> queryToRid(int id) {
        List<Reply> replies = mapper.SelectByToRid(id);
        if (replies == null || replies.isEmpty()){
            return null;
        }
        return replies;
    }

    @Override
    public int addReply(String content, int id, int cid, String time,String image) {
        int num = mapper.addReply(content, id, cid, time,image);
        return num;
    }

    @Override
    public int addReplys(String content, int id, int toRid, String time,String image) {
        int num = mapper.addReplys(content, id, toRid, time,image);
        return num;
    }
}
