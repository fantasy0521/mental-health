package com.mhs.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.mhs.dto.EssayDto;
import com.mhs.entity.Essay;
import com.mhs.mapper.EssayMapper;
import com.mhs.service.IEssayService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mhs.vo.Result;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lyl
 * @since 2023-04-09
 */
@Service
public class EssayServiceImpl extends ServiceImpl<EssayMapper, Essay> implements IEssayService {

    @Resource
    private EssayMapper mapper;

    @Override
    public List<Essay> queryAll() {
        List<Essay> essays = mapper.SelectAll();
        if (essays == null || essays.isEmpty()){
            return null;
        }
        return essays;
    }

    @Override
    public Essay queryId(int id) {
        Essay essay = mapper.SelectByEid(id);
        if (essay == null){
            return null;
        }
        return essay;
    }

    @Override
    public List<EssayDto> queryAllIsCollect(int id) {
        List<EssayDto> essayDtos = mapper.SelectAllByUserId2(id);
        if (essayDtos == null || essayDtos.isEmpty()){
            return null;
        }
        return essayDtos;
    }

    @Override
    public Result deletEssay(Integer eid) {
        LambdaUpdateWrapper<Essay> updateWrapper = new LambdaUpdateWrapper<Essay>();
        updateWrapper.eq(Essay::getEid,eid);
        Essay byId = this.getById(eid);
        if (byId == null) {
            return Result.failure("文章不存在");
        }
        byId.setType("0");
        this.updateById(byId);
        return Result.success("删除成功");
    }

    @Override
    public Result getAllEssay() {
        LambdaQueryWrapper<Essay> queryWrapper = new LambdaQueryWrapper<>();
//        queryWrapper.isNotNull(Essay::getType);
        queryWrapper.ne(Essay::getType,"0");
        List<Essay> list = this.list(queryWrapper);
        return Result.success(list);
    }

}
