package com.mhs.mapper;

import com.mhs.entity.Essay;
import com.mhs.entity.Read;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lyl
 * @since 2023-04-20
 */
public interface ReadMapper extends BaseMapper<Read> {

    @Update("update `read` set iscollect = #{state} where eid = #{id} and id = #{uid}")
    public int Change(@Param("state") int state,
                      @Param("id") int id,
                      @Param("uid") int uid);

    @Insert("insert into `read` values(null,#{uid},#{id},null,#{state})")
    public int Add(@Param("uid") int uid,
                   @Param("id") int id,
                   @Param("state") int state);

    @Update("update `read` set isread = #{state} where eid = #{id} and id = #{uid}")
    public int Alter(@Param("state") int state,
                      @Param("id") int id,
                      @Param("uid") int uid);

    @Insert("insert into `read` values(null,#{uid},#{id},#{state},null)")
    public int Increase(@Param("uid") int uid,
                   @Param("id") int id,
                   @Param("state") int state);


    @Select("select * from `read` where iscollect = 1 and id =#{id}")
    @Results(
            value = {
                    @Result(column = "rid", property = "rid", id = true),
//                    一对多关联查询，是用主表id查询从表
//                    一对一关联查询，是用外键关联字段来查询另一个表
                    @Result(column = "eid", property = "essay", one = @One(
                            select = "SelectByEid"
                    ))
            }
    )
    public List<Read> queryState(int id);

    @Select("select * from essay where eid = #{eid}")
    public Essay SelectByEid(int eid);

//    @Select("select * from read id = #{id}")
//    @Results(
//            value = {
//                    @Result(column = "rid", property = "rid", id = true),
////                    一对多关联查询，是用主表id查询从表
////                    一对一关联查询，是用外键关联字段来查询另一个表
//                    @Result(column = "eid", property = "essays", one = @One(
//                            select = "SelectByEid"
//                    ))
//            }
//    )
//    public List<Read> SelectRecord(int id);
//
//    @Select("select * from essay")
//    public List<Essay> SelectAll();



    @Select("select * from `read` where eid = #{id} and id = #{uid}")
    public Read SelectIsCollect(@Param("id") int id,
                                @Param("uid") int uid);

    @Select("select * from `read` where id = #{uid} and eid = #{id}")
    public Read FindState(@Param("id") int id,
                          @Param("uid") int uid);


}
