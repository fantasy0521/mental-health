package com.mhs.controller;


import com.mhs.entity.Review;
import com.mhs.service.IReviewService;
import com.mhs.service.IReplyService;
import com.mhs.utils.BaseContext;
import com.mhs.vo.Result;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lyl
 * @since 2023-04-11
 */
@RestController
@RequestMapping("/mhs/review")
public class ReviewController {

    @Resource
    private IReviewService service;
    @Resource
    private IReplyService replyService;

//    @CrossOrigin(origins = {"*"})
    @GetMapping("queryEid")
    public Result queryEid(int id){
        List<Review> list = service.queryEid(id);
        for (Review comment : list) {
            int count = replyService.countReply(comment.getCid());
            comment.setCountReply(count);
        }
        if (list == null || list.isEmpty()){
            return new Result(0,"查无数据",null);
        }
        return new Result(1,"查询成功", list);
    }

//    @CrossOrigin(origins = {"*"})
    @GetMapping("addComment")
    public Result addComment(@RequestParam("") String content,
                             @RequestParam("") int eid,
                             @RequestParam("") String image){
        Long currentId = BaseContext.getCurrentId();
        int uid = currentId.intValue();
        LocalDateTime dateTime = LocalDateTime.now(); // get the current date and time
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        System.out.println(dateTime.format(formatter));
        String time = dateTime.format(formatter);
        int num = service.addComment(content, eid, uid, time, image);
        if (num == 0){
            return new Result(0,"添加失败",null);
        }
        return new Result(1,"添加成功", num);
    }
}

