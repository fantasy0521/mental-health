package com.mhs.service;

import com.mhs.entity.Reply;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lyl
 * @since 2023-04-11
 */
public interface IReplyService extends IService<Reply> {


    public List<Reply> queryCid(int id);

    public int countReply(int id);

    public List<Reply> queryToRid(int id);

    public int addReply(String content,int id,int cid,String time,String image);

    public int addReplys(String content,int id,int toRid,String time,String image);
}
