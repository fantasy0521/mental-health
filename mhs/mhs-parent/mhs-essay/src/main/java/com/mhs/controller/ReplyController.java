package com.mhs.controller;


import com.mhs.entity.Reply;
import com.mhs.service.IReplyService;
import com.mhs.utils.BaseContext;
import com.mhs.vo.Result;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lyl
 * @since 2023-04-11
 */
@RestController
@RequestMapping("/mhs/reply")
public class ReplyController {

    @Resource
    private IReplyService service;

//    @CrossOrigin(origins = {"*"})
    @GetMapping("queryCid")
    public Result queryCid(int id){
        List<Reply> list = service.queryCid(id);
        if (list == null || list.isEmpty()){
            return new Result(0,"查无数据", null);
        }
        return new Result(1,"查询成功",list);
    }

//    @CrossOrigin(origins = {"*"})
    @GetMapping("queryToRid")
    public Result queryToRid(int id){
        List<Reply> list = service.queryToRid(id);
        if (list == null || list.isEmpty()){
            return new Result(0,"查无数据", null);
        }
        return new Result(1,"查询成功",list);
    }

//    @CrossOrigin(origins = {"*"})
    @GetMapping("addReply")
    public Result addReply(@RequestParam("") String content,
                           @RequestParam("") int cid,
                           @RequestParam("") String image){
        Long currentId = BaseContext.getCurrentId();
        int uid = currentId.intValue();
        LocalDateTime dateTime = LocalDateTime.now(); // get the current date and time
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        System.out.println(dateTime.format(formatter));
        String time = dateTime.format(formatter);
        int num = service.addReply(content, uid, cid, time, image);
        if (num == 0){
            return new Result(0,"添加失败",null);
        }
        return new Result(1,"添加成功", num);
    }

//    @CrossOrigin(origins = {"*"})
    @GetMapping("addReplys")
    public Result addReplys(@RequestParam("") String content,
                            @RequestParam("") int toRid,
                            @RequestParam("") String image){
        Long currentId = BaseContext.getCurrentId();
        int uid = currentId.intValue();
        LocalDateTime dateTime = LocalDateTime.now(); // get the current date and time
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        System.out.println(dateTime.format(formatter));
        String time = dateTime.format(formatter);
        int num = service.addReplys(content, uid, toRid, time, image);
        if (num == 0){
            return new Result(0,"添加失败",null);
        }
        return new Result(1,"添加成功", num);
    }


}

