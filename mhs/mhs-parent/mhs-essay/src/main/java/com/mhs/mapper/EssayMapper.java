package com.mhs.mapper;

import com.mhs.dto.EssayDto;
import com.mhs.entity.Essay;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mhs.entity.Read;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lyl
 * @since 2023-04-09
 */
public interface EssayMapper extends BaseMapper<Essay> {


    @Select("select * from essay where type is not null")
//    public List<Essay> SelectAll();
    @Results(
            value = {
                    @Result(column = "rid", property = "rid", id = true),
//                    一对多关联查询，是用主表id查询从表
//                    一对一关联查询，是用外键关联字段来查询另一个表
                    @Result(column = "eid", property = "read", one = @One(
                            select = "SelectByRid"
                    ))
            }
    )
    public List<Essay> SelectAll();


    List<EssayDto> SelectAllByUserId(int userId);
    List<EssayDto> SelectAllByUserId2(int userId);

    @Select("select * from `read` where isread = 0")
    public Read SelectByRid(int eid, int uid);




    @Select("select * from essay where eid = #{eid}")
    public Essay SelectByEid(int eid);

}
