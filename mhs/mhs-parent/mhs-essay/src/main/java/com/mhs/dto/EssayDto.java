package com.mhs.dto;

import com.mhs.entity.Essay;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EssayDto extends Essay {
    private Integer iscollect;
}
