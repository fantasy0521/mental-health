package com.mhs.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Result {

    private int code;
    private String msg;
    private Object data;

    public static Result success(String msg,Object data){
        return new Result(1,msg,data);
    }

    public static Result success(String msg){
        return new Result(1,msg,null);
    }

    public static Result success(Object data){
        return new Result(1,null,data);
    }

    public static Result failure(String msg,Object data){
        return new Result(0,msg,data);
    }

    public static Result failure(String msg){
        return new Result(0,msg,null);
    }

}
