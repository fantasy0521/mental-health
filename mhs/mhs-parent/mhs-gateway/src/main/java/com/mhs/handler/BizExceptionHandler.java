package com.mhs.handler;

import com.mhs.vo.Result;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 自定义全局异常拦截器
 * 尝试拦截503错误
 */
@RestControllerAdvice
public class BizExceptionHandler {
    
    @ExceptionHandler(RuntimeException.class)
    public Result runtimeHandler(RuntimeException e){
        e.printStackTrace();
        return Result.failure(e.getMessage());
    }

    @ExceptionHandler(Throwable.class)
    public Result allHandler(Throwable e){
        e.printStackTrace();
        //需要进行记录
        return Result.failure(e.getMessage());
    }

}
