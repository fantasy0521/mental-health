package com.mhs.mapper;

import com.mhs.entity.ArticleImgs;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Fantasy0521
 * @since 2023-04-11
 */
public interface ArticleImgsMapper extends BaseMapper<ArticleImgs> {

}
