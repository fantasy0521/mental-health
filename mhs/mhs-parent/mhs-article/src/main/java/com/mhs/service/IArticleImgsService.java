package com.mhs.service;

import com.mhs.entity.ArticleImgs;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Fantasy0521
 * @since 2023-04-11
 */
public interface IArticleImgsService extends IService<ArticleImgs> {

}
