package com.mhs.service.impl;

import com.mhs.entity.ArticleImgs;
import com.mhs.mapper.ArticleImgsMapper;
import com.mhs.service.IArticleImgsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Fantasy0521
 * @since 2023-04-11
 */
@Service
public class ArticleImgsServiceImpl extends ServiceImpl<ArticleImgsMapper, ArticleImgs> implements IArticleImgsService {

}
