package com.mhs.mapper;

import com.mhs.entity.Comment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mhs.vo.CommentVo;

import java.util.List;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author Fantasy0521
 * @since 2023-04-12
 */
public interface CommentMapper extends BaseMapper<Comment> {

    List<CommentVo> selectCommentsByParentCommentId(Long articleId, Long parentCommentId);
    
}
