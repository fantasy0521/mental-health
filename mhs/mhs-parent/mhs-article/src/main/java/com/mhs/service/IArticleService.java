package com.mhs.service;

import com.mhs.entity.Article;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mhs.vo.ArticleVo;
import com.mhs.vo.Result;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Fantasy0521
 * @since 2023-04-09
 */
public interface IArticleService extends IService<Article> {

    Result send(ArticleVo article);

    Result getArticleListByPage(Integer page, Integer pageSize);

    Result getArticleById(Integer articleId);


    Result pick(Integer articleId);
}
