package com.mhs.vo;

import com.mhs.entity.Comment;
import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CommentVo extends Comment {
    
    //回复该评论的的评论
    private List<CommentVo> replyComments;
    
    //该评论的作者
    //用户昵称
    private String nickName;
    //头像
    private String avatar;
    
}
