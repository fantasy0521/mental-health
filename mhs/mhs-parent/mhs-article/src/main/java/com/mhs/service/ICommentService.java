package com.mhs.service;

import com.mhs.entity.Comment;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mhs.vo.Result;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Fantasy0521
 * @since 2023-04-12
 */
public interface ICommentService extends IService<Comment> {

    Result getCommentsAndReply(Long articleId, Integer page, Integer pageSize);

    Result addComment(Comment comment);
}
