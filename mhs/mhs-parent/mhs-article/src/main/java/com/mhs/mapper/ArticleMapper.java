package com.mhs.mapper;

import com.mhs.entity.Article;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mhs.vo.ArticleVo;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Fantasy0521
 * @since 2023-04-09
 */
public interface ArticleMapper extends BaseMapper<Article> {

    List<ArticleVo> selectPageOrder(int start, Integer pageSize);

    void updatePick(Integer articleId);
}
