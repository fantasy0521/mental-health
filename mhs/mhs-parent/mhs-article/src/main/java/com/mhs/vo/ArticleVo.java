package com.mhs.vo;

import com.mhs.entity.Article;
import com.mhs.entity.Comment;
import com.mhs.entity.User;
import lombok.*;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ArticleVo extends Article {
    
    //所有图片
    private List<String> imgs;
    
    //该文章下的所有评论
    private List<Comment> comments;
    
    //该文章下的评论数
    private Integer commentCount;
    
    //该文章的作者
    //用户昵称
    private String nickName;
    //头像
    private String avatar;
    
    
}
