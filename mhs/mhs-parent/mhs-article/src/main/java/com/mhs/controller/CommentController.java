package com.mhs.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.mhs.entity.Comment;
import com.mhs.service.ICommentService;
import com.mhs.vo.Result;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import javax.annotation.Resource;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author Fantasy0521
 * @since 2023-04-12
 */
@RestController
@RequestMapping("/mhs/comment")
public class CommentController {


    @Resource
    private ICommentService commentService;
    
    /**
     * 根据文章id分页查询评论列表
     * @param articleId
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping("getComments")
    public Result getComments(@RequestParam(required = false) Long articleId,
                              @RequestParam(defaultValue = "0") Integer page,
                              @RequestParam(defaultValue = "10") Integer pageSize) {
        return commentService.getCommentsAndReply(articleId,page,pageSize);
    }

    /**
     * 添加评论
     * @param comment
     * @return
     */
    @PostMapping("addComment")
    public Result addComment(@RequestBody Comment comment){//todo 进行表单验证
        return commentService.addComment(comment);
    }

    @PutMapping
    public Result editComment(@RequestBody Comment comment){
        commentService.updateById(comment);
        return Result.success("修改成功");
    }
    @DeleteMapping
    public Result deleteComment(Integer id){
        commentService.removeById(id);
        LambdaQueryWrapper<Comment> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Comment::getParentCommentId,id);
        commentService.remove(queryWrapper);
        return Result.success("删除成功");
    }

}

