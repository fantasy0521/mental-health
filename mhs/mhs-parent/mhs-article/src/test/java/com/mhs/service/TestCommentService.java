package com.mhs.service;

import com.mhs.vo.Result;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
public class TestCommentService {
    
    @Resource
    private ICommentService commentService;
    
    @Test
    public void testGetCommentsAndReply(){
        Result commentsAndReply = commentService.getCommentsAndReply(1L, 1, 5);
        System.out.println(commentsAndReply);
    }
    
}
