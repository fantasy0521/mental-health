package com.mhs.service;

import com.mhs.mapper.CommentMapper;
import com.mhs.vo.CommentVo;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.List;

@SpringBootTest
public class TestCommentMapper {
    
    @Resource
    private CommentMapper commentMapper;
    
    @Test
    public void testSelectCommentsByParentCommentId(){
        List<CommentVo> commentVos = commentMapper.selectCommentsByParentCommentId(1L, -1L);
        System.out.println(commentVos);
    }
    
}
