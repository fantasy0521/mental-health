package com.mhs.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mhs.vo.ArticleVo;
import com.mhs.vo.Result;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class TestArticleService {
    
    @Autowired
    private IArticleService articleService;
    
    @Test
    public void testGetListByPage(){
        Result articleListByPage = articleService.getArticleListByPage(1, 5);
        Page<ArticleVo> data = (Page<ArticleVo>) articleListByPage.getData();
        List<ArticleVo> records = data.getRecords();
        System.out.println(records);
    }
    
    
    
}
