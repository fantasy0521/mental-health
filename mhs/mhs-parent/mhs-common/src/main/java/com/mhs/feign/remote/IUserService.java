package com.mhs.feign.remote;

import com.mhs.entity.User;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 远程调用user接口
 */
@FeignClient(value = "mhs-user",fallback = UserServiceImpl.class)
public interface IUserService {

    /**
     * 通过userId获取User
     * @param userId
     * @return
     */
    @GetMapping("/mhs/user/getUserById")
    public User getUserById(@RequestParam Long userId);
    
}
