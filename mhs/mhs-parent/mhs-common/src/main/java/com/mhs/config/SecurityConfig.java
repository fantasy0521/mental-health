package com.mhs.config;

import com.mhs.filter.JwtAuthenticationTokenFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.annotation.Resource;

/**
 * SpringSecurity配置类
 */
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true) //开启授权注解功能
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    //创建BCryptPasswordEncoder注入容器
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Resource
    private JwtAuthenticationTokenFilter jwtAuthenticationTokenFilter;

    @Resource
    private AuthenticationEntryPoint authenticationEntryPoint;
    
    @Resource
    private AccessDeniedHandler accessDeniedHandler;
    
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                //前后端分离可以关闭对csrf攻击的抵御,csrf攻击依靠cookie而不是token
                .csrf().disable() 
                //不通过Session获取SecurityContext (前后端分离)
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                // 对于登录接口 允许匿名访问 anonymous匿名访问,已登陆就不能访问
                //.permitAll() 登陆和未登录都放行
                .antMatchers("/mhs/user/loginByWeixin").anonymous() //将微信登陆放行
                .antMatchers("/mhs/user/login").anonymous() //将登陆放行
                .antMatchers("/mhs/user/register").permitAll() //将注册放行
                .antMatchers("/mhs/article/download").permitAll() //将下载放行
                .antMatchers("/mhs/user/nacos").permitAll() //将下载放行
                .antMatchers("/mhs/user/loginAdmin").permitAll() //将管理员登陆放行
                .antMatchers("/instances/**").permitAll() //将监控放行
                .antMatchers("/8092/**").permitAll() //将监控放行
                .antMatchers("/logout").permitAll() //将推出监控放行
                .antMatchers("/applications/**").permitAll() //将推出监控放行
                .antMatchers("/mhs/chat/**").permitAll() //将推出监控放行
                //可以在这里进行权限控制
                //.antMatchers("/testCors").hasAuthority("system:dept:list222")
                // 除上面外的所有请求全部需要鉴权认证
                .anyRequest().authenticated();
        //将自定义的JWTFilter加入到Security过滤器链中,并指定位置
        http.addFilterBefore(jwtAuthenticationTokenFilter, UsernamePasswordAuthenticationFilter.class);
        
        //配置异常处理器
        http.exceptionHandling()
                //配置认证失败处器
                .authenticationEntryPoint(authenticationEntryPoint)
                //配置授权失败处理器
                .accessDeniedHandler(accessDeniedHandler);
        
        //允许跨域
        http.cors();
    }
}
