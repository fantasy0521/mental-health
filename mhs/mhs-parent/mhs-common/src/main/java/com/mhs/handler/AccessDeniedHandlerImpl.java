package com.mhs.handler;

import com.alibaba.fastjson.JSON;
import com.mhs.utils.WebUtils;
import com.mhs.vo.Result;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 授权错误异常处理器
 */
@Component
public class AccessDeniedHandlerImpl implements AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response, AccessDeniedException accessDeniedException) throws IOException, ServletException {
        //处理异常 HttpStatus.FORBIDDEN.value() = 403
        Result result = new Result(HttpStatus.FORBIDDEN.value(),"授权失败",null);
        String json = JSON.toJSONString(result);
        WebUtils.renderString(response,json);
    }
}
