package com.mhs.config;

/**
 * 权限等级枚举值
 */
public enum Authorization {
    
    ADMIN, //管理员
    DOCTOR, //医生
    USER //用户
    
}
