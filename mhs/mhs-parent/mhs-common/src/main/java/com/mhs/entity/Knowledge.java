package com.mhs.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author lyl
 * @since 2023-04-06
 */
public class Knowledge implements Serializable {

    private static final long serialVersionUID = 1L;

      @TableId(value = "kid", type = IdType.AUTO)
      private Integer kid;

    private String title;

    private String content;

    
    public Integer getKid() {
        return kid;
    }

      public void setKid(Integer kid) {
          this.kid = kid;
      }
    
    public String getTitle() {
        return title;
    }

      public void setTitle(String title) {
          this.title = title;
      }
    
    public String getContent() {
        return content;
    }

      public void setContent(String content) {
          this.content = content;
      }

    @Override
    public String toString() {
        return "Knowledge{" +
              "kid=" + kid +
                  ", title=" + title +
                  ", content=" + content +
              "}";
    }
}
