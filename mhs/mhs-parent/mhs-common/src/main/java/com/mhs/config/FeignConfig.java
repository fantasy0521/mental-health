package com.mhs.config;

import com.mhs.handler.MyFeignClientInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * feign配置类
 */
@Configuration
public class FeignConfig {

    //注册Feign拦截器 解决远程调用验证问题
    @Bean
    public MyFeignClientInterceptor feignClientInterceptor() {
        return new MyFeignClientInterceptor();
    }

}
