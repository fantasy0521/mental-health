package com.mhs.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author Fantasy0521
 * @since 2023-04-12
 */
public class Article implements Serializable {

    private static final long serialVersionUID = 1L;

      @TableId(value = "id", type = IdType.AUTO)
      private Long id;

      /**
     * 文章标题
     */
      private String title;

      /**
     * 文章首图，用于随机文章展示
     */
      private String firstPicture;

      /**
     * 文章正文
     */
      private String content;

      /**
     * 描述
     */
      private String description;

      /**
     * 公开或私密
     */
      private Boolean isPublished;

      /**
     * 创建时间
     */
      private LocalDateTime createTime;

      /**
     * 浏览次数
     */
      private Integer views;

      /**
     * 是否置顶
     */
      private Boolean isTop;

      /**
     * 文章作者
     */
      private Long userId;

      /**
     * 点赞数
     */
      private Integer pick;

    
    public Long getId() {
        return id;
    }

      public void setId(Long id) {
          this.id = id;
      }
    
    public String getTitle() {
        return title;
    }

      public void setTitle(String title) {
          this.title = title;
      }
    
    public String getFirstPicture() {
        return firstPicture;
    }

      public void setFirstPicture(String firstPicture) {
          this.firstPicture = firstPicture;
      }
    
    public String getContent() {
        return content;
    }

      public void setContent(String content) {
          this.content = content;
      }
    
    public String getDescription() {
        return description;
    }

      public void setDescription(String description) {
          this.description = description;
      }
    
    public Boolean getIsPublished() {
        return isPublished;
    }

      public void setIsPublished(Boolean isPublished) {
          this.isPublished = isPublished;
      }
    
    public LocalDateTime getCreateTime() {
        return createTime;
    }

      public void setCreateTime(LocalDateTime createTime) {
          this.createTime = createTime;
      }
    
    public Integer getViews() {
        return views;
    }

      public void setViews(Integer views) {
          this.views = views;
      }
    
    public Boolean getIsTop() {
        return isTop;
    }

      public void setIsTop(Boolean isTop) {
          this.isTop = isTop;
      }
    
    public Long getUserId() {
        return userId;
    }

      public void setUserId(Long userId) {
          this.userId = userId;
      }
    
    public Integer getPick() {
        return pick;
    }

      public void setPick(Integer pick) {
          this.pick = pick;
      }

    @Override
    public String toString() {
        return "Article{" +
              "id=" + id +
                  ", title=" + title +
                  ", firstPicture=" + firstPicture +
                  ", content=" + content +
                  ", description=" + description +
                  ", isPublished=" + isPublished +
                  ", createTime=" + createTime +
                  ", views=" + views +
                  ", isTop=" + isTop +
                  ", userId=" + userId +
                  ", pick=" + pick +
              "}";
    }
}
