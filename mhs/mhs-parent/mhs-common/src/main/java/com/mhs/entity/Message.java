package com.mhs.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.util.Date;


/**
* 
* @TableName message
*/
public class Message implements Serializable {

    /**
    * 
    */
    @TableId( type = IdType.AUTO)
    private Long messageId;
    /**
    * 
    */
    private Long chatId;
    /**
    * 
    */

    private String content;
    /**
    * 
    */
    private Long fromUid;
    /**
    * 
    */
    private Long toUid;
    /**
    * 
    */
    public LocalDateTime creatTime;

    /**
    * 
    */
    public void setMessageId(Long messageId){
    this.messageId = messageId;
    }

    /**
    * 
    */
    public void setChatId(Long chatId){
    this.chatId = chatId;
    }

    /**
    * 
    */
    public void setContent(String content){
    this.content = content;
    }

    /**
    * 
    */
    public void setFromUid(Long fromUid){
    this.fromUid = fromUid;
    }

    /**
    * 
    */
    public void setToUid(Long toUid){
    this.toUid = toUid;
    }

    /**
    * 
    */
    public void setCreatTime(LocalDateTime creatTime){
    this.creatTime = creatTime;
    }


    /**
    * 
    */
    public Long getMessageId(){
    return this.messageId;
    }

    /**
    * 
    */
    public Long getChatId(){
    return this.chatId;
    }

    /**
    * 
    */
    public String getContent(){
    return this.content;
    }

    /**
    * 
    */
    public Long getFromUid(){
    return this.fromUid;
    }

    /**
    * 
    */
    public Long getToUid(){
    return this.toUid;
    }

    /**
    * 
    */
    public LocalDateTime getCreatTime(){
    return this.creatTime;
    }

}
