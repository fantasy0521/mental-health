package com.mhs.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author Fantasy0521
 * @since 2023-04-11
 */
@TableName("article_imgs")
public class ArticleImgs implements Serializable {

    private static final long serialVersionUID = 1L;

      @TableId(value = "id", type = IdType.AUTO)
      private Long id;

      /**
     * 图片
     */
      private String img;

      /**
     * 所属文章
     */
      private Long articleId;

    
    public Long getId() {
        return id;
    }

      public void setId(Long id) {
          this.id = id;
      }
    
    public String getImg() {
        return img;
    }

      public void setImg(String img) {
          this.img = img;
      }
    
    public Long getArticleId() {
        return articleId;
    }

      public void setArticleId(Long articleId) {
          this.articleId = articleId;
      }

    @Override
    public String toString() {
        return "ArticleImgs{" +
              "id=" + id +
                  ", img=" + img +
                  ", articleId=" + articleId +
              "}";
    }
}
