package com.mhs.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 
 * </p>
 *
 * @author lyl
 * @since 2023-04-20
 */
@Data
public class Read implements Serializable {

    private Essay essay;

    private static final long serialVersionUID = 1L;

      @TableId(value = "rid", type = IdType.AUTO)
      private Integer rid;

      /**
     * 用户id
     */
      private Integer id;

      /**
     * 文章id
     */
      private Integer eid;

      /**
     * 是否被阅读
     */
      private Integer isread;

      /**
     * 是否被收藏
     */
      private Integer iscollect;

    
    public Integer getRid() {
        return rid;
    }

      public void setRid(Integer rid) {
          this.rid = rid;
      }
    
    public Integer getId() {
        return id;
    }

      public void setId(Integer id) {
          this.id = id;
      }
    
    public Integer getEid() {
        return eid;
    }

      public void setEid(Integer eid) {
          this.eid = eid;
      }
    
    public Integer getIsread() {
        return isread;
    }

      public void setIsread(Integer isread) {
          this.isread = isread;
      }
    
    public Integer getIscollect() {
        return iscollect;
    }

      public void setIscollect(Integer iscollect) {
          this.iscollect = iscollect;
      }

    @Override
    public String toString() {
        return "Read{" +
              "rid=" + rid +
                  ", id=" + id +
                  ", eid=" + eid +
                  ", isread=" + isread +
                  ", iscollect=" + iscollect +
              "}";
    }
}
