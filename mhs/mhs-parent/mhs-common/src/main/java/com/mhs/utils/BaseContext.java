package com.mhs.utils;

/**
 * 基于ThreadLocal封装工具类,用户保存和获取当前登陆用户id
 * 作用于同一个线程,浏览器每次发送一个请求就是一个独立的线程
 */
public class BaseContext {
    private static ThreadLocal<Long> threadLocal = new ThreadLocal<>();

    public static void setCurrentId(Long id){
        threadLocal.set(id);
    }
    
    public static Long getCurrentId(){
        return threadLocal.get();
    }
}
