package com.mhs.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author lyl
 * @since 2023-04-11
 */
@Data
public class Reply implements Serializable {

    private User user;

    private static final long serialVersionUID = 1L;

      @TableId(value = "rid", type = IdType.AUTO)
      private Integer rid;

    private String content;

      /**
     * 回复人的id
     */
      private Integer id;

      /**
     * 回复的回复id
     */
      private Integer toRid;

      private Integer cid;

      private LocalDateTime createTime;

      private String image;


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getRid() {
        return rid;
    }

    public void setRid(Integer rid) {
        this.rid = rid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getToRid() {
        return toRid;
    }

    public void setToRid(Integer toRid) {
        this.toRid = toRid;
    }

    public Integer getCid() {
        return cid;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Reply{" +
                "rid=" + rid +
                ", content='" + content + '\'' +
                ", id=" + id +
                ", toRid=" + toRid +
                ", cid=" + cid +
                ", createTime=" + createTime +
                '}';
    }
}
