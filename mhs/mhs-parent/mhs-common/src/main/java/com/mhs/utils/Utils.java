package com.mhs.utils;


import com.mhs.exception.BizException;

import java.util.Collection;
import java.util.Map;

public class Utils {

    /**
     * 判断空值: 空对象,空字符串,空集合
     * @param obj
     * @return
     */
    public static boolean isEmpty(Object obj) {
        if (obj == null) {
            return true;
        } else if (obj instanceof String) {
            return ((String) obj).trim().isEmpty();
        } else if (obj instanceof Collection) {
            return ((Collection) obj).isEmpty();
        } else if (obj instanceof Map) {
            return ((Map) obj).isEmpty();
        }
        return false;
    }

    /**
     * 用于检查bool值是否是true ,如果是true ,则抛出异常
     * @param bool
     * @param msg
     * @throws BizException
     */
    public static void check(boolean bool,String msg) throws BizException {
        if (bool){
            throw new BizException(msg);
        }
    }

    /**
     * 用于检查obj值是否是空,如果是空,则抛出异常
     * @param obj
     * @param msg
     */
    public static void checkEmpty(Object obj,String msg) throws BizException {
        check(isEmpty(obj),msg);
    }

}