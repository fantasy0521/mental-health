package com.mhs.handler;

import com.alibaba.fastjson.JSON;
import com.mhs.utils.WebUtils;
import com.mhs.vo.Result;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 认证错误异常处理器
 */
@Component
public class AuthenticationEntryPointImpl implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        //处理异常 HttpStatus.UNAUTHORIZED.value() = 401
        Result result = new Result(HttpStatus.UNAUTHORIZED.value(),"用户认证失败",null);
        String json = JSON.toJSONString(result);
        WebUtils.renderString(response,json);
    }
    
}
