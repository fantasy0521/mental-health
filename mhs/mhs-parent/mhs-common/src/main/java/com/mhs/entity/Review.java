package com.mhs.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author lyl
 * @since 2023-04-11
 */
@Data
@TableName("review")
public class Review implements Serializable {

    private User user;

    private static final long serialVersionUID = 1L;

      /**
     * 评论id
     */
        @TableId(value = "cid", type = IdType.AUTO)
      private Integer cid;

    private String content;

      /**
     * 文章id
     */
      private Integer eid;

      /**
     * 评论人id
     */
      private Integer id;

      private Integer countReply;

      private LocalDateTime createTime;

      private String image;


    public Integer getCid() {
        return cid;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getEid() {
        return eid;
    }

    public void setEid(Integer eid) {
        this.eid = eid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCountReply() {
        return countReply;
    }

    public void setCountReply(Integer countReply) {
        this.countReply = countReply;
    }

    public LocalDateTime getCreateTime() {
        return createTime;
    }

    public void setCreateTime(LocalDateTime createTime) {
        this.createTime = createTime;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
