package com.mhs.entity;


import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
* 
* @TableName option
*/
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("option")
public class Option implements Serializable {

    /**
    * 选项id
    */
   
    private Long oid;
    /**
    * 选项属于的问题的id
    */
    private Long qid;
    /**
    * 选项
    */
 
    private String miaoshu;
    /**
    * 该选项的得分
    */
  
    private Integer score;

    /**
    * 选项id
    */
    public void setOid(Long oid){
    this.oid = oid;
    }

    /**
    * 选项属于的问题的id
    */
    public void setQid(Long qid){
    this.qid = qid;
    }

    /**
    * 选项
    */
    public void setMiaoshu(String miaoshu){
    this.miaoshu = miaoshu;
    }

    /**
    * 该选项的得分
    */
    public void setScore(Integer score){
    this.score = score;
    }


    /**
    * 选项id
    */
    public Long getOid(){
    return this.oid;
    }

    /**
    * 选项属于的问题的id
    */
    public Long getQid(){
    return this.qid;
    }

    /**
    * 选项
    */
    public String getMiaoshu(){
    return this.miaoshu;
    }

    /**
    * 该选项的得分
    */
    public Integer getScore(){
    return this.score;
    }

}
