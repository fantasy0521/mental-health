//package com.mhs.utils;
//
//import com.baomidou.mybatisplus.generator.FastAutoGenerator;
//import com.baomidou.mybatisplus.generator.config.OutputFile;
//import com.baomidou.mybatisplus.generator.config.po.LikeTable;
//
//import java.util.Collections;
//
//public class MyBatisCoder {
//    public static void main(String[] args) {
//        FastAutoGenerator.create("jdbc:mysql://127.0.0.1:3306/mhs", "root", "a")
//                .globalConfig(builder -> {
//                    builder.author("lyl") // 设置作者
//                            // .enableSwagger() // 开启 swagger 模式 自动生成api文档 html格式
//                            .fileOverride() // 覆盖已生成文件 数据结构发生变化 需要重新生成代码
//                            .outputDir("C:\\Users\\念流年\\Documents\\Browser Files"); // 指定输出目录
//                })
//                .packageConfig(builder -> {
//                    builder.parent("com.mhs") // 设置父包名
//                            .moduleName("mentalhealth") // 设置父包模块名
//                            .pathInfo(Collections.singletonMap(OutputFile.mapperXml, "C:\\Users\\念流年\\Documents\\Browser Files")); // 设置mapperXml生成路径 既生成mapper.xml文件
//                })
//                .strategyConfig(builder -> {
////                    builder.addInclude("t_simple") // 设置需要生成的表名 指定一个表
////                            .addTablePrefix("t_", "c_"); // 设置过滤表前缀
//                    builder.likeTable(new LikeTable("%")); // 使用模糊匹配来匹配表  生成全部表
//                })
//                // 设置生成代码使用的模板技术 切换模板技术 替换类型
//                // .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
//                .execute();
//    }
//}
