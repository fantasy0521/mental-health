package com.mhs.entity;



import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import java.io.Serializable;

import java.time.LocalDateTime;
import java.util.Date;


/**
* 
* @TableName chat
*/
@NoArgsConstructor
@AllArgsConstructor
public class Chat implements Serializable {

    /**
    * 
    */

    @TableId(value = "chat_id",type = IdType.AUTO)
    private Long chatId;
    /**
    * 
    */
    private Long consultantId;
    /**
    * 
    */
    private Long consumerId;
    /**
    * 
    */
    private LocalDateTime createTime;

    /**
    * 
    */
    public void setChatId(Long chatId){
    this.chatId = chatId;
    }

    /**
    * 
    */
    public void setConsultantId(Long consultantId){
    this.consultantId = consultantId;
    }

    /**
    * 
    */
    public void setConsumerId(Long consumerId){
    this.consumerId = consumerId;
    }

    /**
    * 
    */
    public void setCreateTime(LocalDateTime createTime){
    this.createTime = createTime;
    }


    /**
    * 
    */
    public Long getChatId(){
    return this.chatId;
    }

    /**
    * 
    */
    public Long getConsultantId(){
    return this.consultantId;
    }

    /**
    * 
    */
    public Long getConsumerId(){
    return this.consumerId;
    }

    /**
    * 
    */
    public LocalDateTime getCreateTime(){
    return this.createTime;
    }

}
