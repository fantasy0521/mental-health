package com.mhs.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.util.List;


/**
 * @TableName quiz
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Quiz implements Serializable {

    /**
     *
     */
    private List<Option> options;
    private Long qid;
    /**
     *
     */

    private String tid;
    /**
     *
     */
    private Integer qNum;
    /**
     *
     */

    private String question;

    /**
     *
     */
    public void setQid(Long qid) {
        this.qid = qid;
    }

    /**
     *
     */
    public void setTid(String tid) {
        this.tid = tid;
    }

    /**
     *
     */
    public void setQNum(Integer qNum) {
        this.qNum = qNum;
    }

    /**
     *
     */
    public void setQuestion(String question) {
        this.question = question;
    }


    /**
     *
     */
    public Long getQid() {
        return this.qid;
    }

    /**
     *
     */
    public String getTid() {
        return this.tid;
    }

    /**
     *
     */
    public Integer getQNum() {
        return this.qNum;
    }

    /**
     *
     */
    public String getQuestion() {
        return this.question;
    }

}
