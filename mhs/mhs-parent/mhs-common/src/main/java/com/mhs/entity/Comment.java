package com.mhs.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 
 * </p>
 *
 * @author Fantasy0521
 * @since 2023-04-12
 */
public class Comment implements Serializable {

    private static final long serialVersionUID = 1L;

      @TableId(value = "id", type = IdType.AUTO)
      private Long id;

      /**
     * 评论内容
     */
      private String content;

      /**
     * 评论时间
     */
      private LocalDateTime createTime;

      /**
     * 评论者ip地址
     */
      private String ip;

      /**
     * 公开或删除
     */
      private Boolean isDeleted;

      /**
     * 评论人
     */
      private Long userId;

      /**
     * 所属的文章
     */
      private Long articleId;

      /**
     * 父评论id，-1为根评论
     */
      private Long parentCommentId;

    
    public Long getId() {
        return id;
    }

      public void setId(Long id) {
          this.id = id;
      }
    
    public String getContent() {
        return content;
    }

      public void setContent(String content) {
          this.content = content;
      }
    
    public LocalDateTime getCreateTime() {
        return createTime;
    }

      public void setCreateTime(LocalDateTime createTime) {
          this.createTime = createTime;
      }
    
    public String getIp() {
        return ip;
    }

      public void setIp(String ip) {
          this.ip = ip;
      }
    
    public Boolean getIsDeleted() {
        return isDeleted;
    }

      public void setIsDeleted(Boolean isDeleted) {
          this.isDeleted = isDeleted;
      }
    
    public Long getUserId() {
        return userId;
    }

      public void setUserId(Long userId) {
          this.userId = userId;
      }
    
    public Long getArticleId() {
        return articleId;
    }

      public void setArticleId(Long articleId) {
          this.articleId = articleId;
      }
    
    public Long getParentCommentId() {
        return parentCommentId;
    }

      public void setParentCommentId(Long parentCommentId) {
          this.parentCommentId = parentCommentId;
      }

    @Override
    public String toString() {
        return "Comment{" +
              "id=" + id +
                  ", content=" + content +
                  ", createTime=" + createTime +
                  ", ip=" + ip +
                  ", isDeleted=" + isDeleted +
                  ", userId=" + userId +
                  ", articleId=" + articleId +
                  ", parentCommentId=" + parentCommentId +
              "}";
    }
}
