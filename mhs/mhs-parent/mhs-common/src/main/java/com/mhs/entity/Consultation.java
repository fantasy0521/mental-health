package com.mhs.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author lyl
 * @since 2023-04-18
 */
@Data
public class Consultation implements Serializable {

    @TableField(exist = false)
    private User user;

    private static final long serialVersionUID = 1L;

      /**
     * 咨询师id
     */
        @TableId(value = "cid", type = IdType.AUTO)
      private Integer cid;

      /**
     * 个人介绍
     */
      private String introduce;

      /**
     * 擅长领域
     */
      private String territory;

      /**
     * 咨询人次
     */
      private Integer number;

      /**
     * 从业年数
     */
      private String year;

      /**
     * 地址
     */
      private String address;

    /**
     * 用户id
     */
    private Integer id;


    /**
     * 审核  0 为待审核  1 为审核成功
     */
    private Integer state;


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getCid() {
        return cid;
    }

    public void setCid(Integer cid) {
        this.cid = cid;
    }

    public String getIntroduce() {
        return introduce;
    }

    public void setIntroduce(String introduce) {
        this.introduce = introduce;
    }

    public String getTerritory() {
        return territory;
    }

    public void setTerritory(String territory) {
        this.territory = territory;
    }

    public Integer getNumber() {
        return number;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getState() {
        return state;
    }

    public void setState(Integer state) {
        this.state = state;
    }


}
