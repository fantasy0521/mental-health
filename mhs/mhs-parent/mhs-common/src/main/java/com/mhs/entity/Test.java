package com.mhs.entity;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;


/**
* 
* @TableName test
*/
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Test implements Serializable {

    /**
    * 
    */
    private List<Quiz> quizzes;
    private Integer tid;
    /**
    * 
    */
   
    private String tTitle;
    /**
    * 
    */
   
    private String tDesc;
    /**
    * 
    */
   
    private String tTotal;
    private float tXishu;
    private Integer tZhuyi;

    private Integer tZixun;

    public float gettXishu() {
        return tXishu;
    }

    public void settXishu(float tXishu) {
        this.tXishu = tXishu;
    }

    public Integer gettZhuyi() {
        return tZhuyi;
    }

    public void settZhuyi(Integer tZhuyi) {
        this.tZhuyi = tZhuyi;
    }

    public Integer gettZixun() {
        return tZixun;
    }

    public void settZixun(Integer tZixun) {
        this.tZixun = tZixun;
    }

    /**
    * 
    */
    public void setTid(Integer tid){
    this.tid = tid;
    }

    /**
    * 
    */
    public void setTTitle(String tTitle){
    this.tTitle = tTitle;
    }

    /**
    * 
    */
    public void setTDesc(String tDesc){
    this.tDesc = tDesc;
    }

    /**
    * 
    */
    public void setTTotal(String tTotal){
    this.tTotal = tTotal;
    }


    /**
    * 
    */
    public Integer getTid(){
    return this.tid;
    }

    /**
    * 
    */
    public String getTTitle(){
    return this.tTitle;
    }

    /**
    * 
    */
    public String getTDesc(){
    return this.tDesc;
    }

    /**
    * 
    */
    public String getTTotal(){
    return this.tTotal;
    }

}
