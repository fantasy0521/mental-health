package com.mhs.handler;

import com.mhs.exception.BizException;
import com.mhs.vo.Result;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * 自定义全局异常拦截器
 */
@RestControllerAdvice
public class BizExceptionHandler {
    
    @ExceptionHandler(BizException.class)
    public Result bizHandler(BizException e){
        e.printStackTrace();
        return Result.failure(e.getMessage());
    }
    
    @ExceptionHandler(RuntimeException.class)
    public Result runtimeHandler(RuntimeException e){
        e.printStackTrace();
        return Result.failure(e.getMessage());
    }

    @ExceptionHandler(Throwable.class)
    public Result allHandler(Throwable e){
        e.printStackTrace();
        //需要进行记录
        return Result.failure(e.getMessage());
    }

}
