package com.mhs;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude= {SecurityAutoConfiguration.class })
//@SpringBootApplication
@MapperScan("com.mhs.mapper")
public class QuizApp {
    //考试结果记录
    public static void main(String[] args) {
        SpringApplication.run(QuizApp.class,args);
    }
}
