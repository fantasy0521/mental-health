package com.mhs.service;

import com.mhs.entity.Test;
import com.mhs.vo.TestSubmitVO;

import java.util.List;

public interface ITestService {

    //获取试卷
    public Test getTest(int tid);
    public List<Test> getTestList();

    int submitTest(TestSubmitVO testSubmitVO);

}
