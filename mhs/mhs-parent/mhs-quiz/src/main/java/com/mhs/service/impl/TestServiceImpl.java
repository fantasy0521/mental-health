package com.mhs.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mhs.entity.Option;
import com.mhs.entity.Quiz;
import com.mhs.entity.Test;
import com.mhs.mapper.ITestMapper;
import com.mhs.service.ITestService;
import com.mhs.vo.TestSubmitVO;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@Service
public class TestServiceImpl implements ITestService {
    @Resource
    ITestMapper testMapper;

    @Override
    public Test getTest(int tid) {

      Optional<Test>  test=  Optional.ofNullable( testMapper.getTestByTid(tid));
               test.orElseThrow(() -> new RuntimeException("没找到这套试卷 optional"));

        return test.get();
    }

    @Override
    public List<Test> getTestList() {

        List<Test> testList = testMapper.getTestList();
        return testList;
    }

    @Override
    public int submitTest(TestSubmitVO testSubmitVO) {
        int score = 0;
        Integer id = testSubmitVO.getId();
        if (id == null) {
            return -1;
        }
        Test test = getTest(id);
        List<Quiz> quizzes = test.getQuizzes();
        List<Integer> chooses = testSubmitVO.getChooses();
        for (Integer choose : chooses) {
            if (choose <= 0){
                return -1;
            }
        }
        for (int i = 0; i < quizzes.size(); i++) {
            Quiz quiz = quizzes.get(i);
            //选项列表
            List<Option> options = quiz.getOptions();
            //所选选项
            Integer choose = chooses.get(i);
            //选项分数
            score += options.get(choose - 1).getScore();
        }
        //分数越高心理问题越严重，这里取个反，与前端保持一致
        return 100 - score;
    }
}


