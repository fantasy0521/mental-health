package com.mhs.vo;

import lombok.Data;

import java.util.List;

@Data
public class TestSubmitVO {

    private Integer id;

    private List<Integer> chooses;

}
