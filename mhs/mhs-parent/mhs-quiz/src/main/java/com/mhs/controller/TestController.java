package com.mhs.controller;

import com.mhs.entity.Test;
import com.mhs.service.impl.TestServiceImpl;
import com.mhs.vo.Result;
import com.mhs.vo.TestSubmitVO;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("mhs/test")

public class TestController {
    @Resource
    TestServiceImpl testService;

//    @CrossOrigin(origins = {"*"})
    @RequestMapping(value = "getTest/{tid}", method = RequestMethod.GET)
    public Result getQuiz(@PathVariable("tid") int tid) {

        Test test = testService.getTest(tid);

        return new Result(200, "成功查询", test);


    }

    @RequestMapping(value = "getTestList",method = RequestMethod.GET)
    public Result getTestList(){
        List<Test> testList = testService.getTestList();
        return new Result(200,"成功查询",testList);
    }

    @PostMapping("submitTest")
    public Result submitTest(@RequestBody TestSubmitVO testSubmitVO){
        int score = testService.submitTest(testSubmitVO);
        if (score == -1){
            return Result.failure("请填写所有题目后再提交");
        }
        return Result.success("成功提交",score);
    }
}
