package com.mhs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mhs.entity.Option;

public interface IOptionMapper extends BaseMapper<Option> {
}
