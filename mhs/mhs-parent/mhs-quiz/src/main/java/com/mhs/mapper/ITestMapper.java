package com.mhs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mhs.entity.Option;
import com.mhs.entity.Quiz;
import com.mhs.entity.Test;
import org.apache.ibatis.annotations.Many;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface ITestMapper extends BaseMapper<Test> {

    @Select("select * from test")
    public List<Test> getTestList();


    @Select("select * from test where tid = #{tid}")
    @Results({
            @Result(column = "tid", property = "tid"),
            @Result(column = "tid", property = "quizzes", many = @Many(select = "getQuizByTid"))
    })
    public Test getTestByTid(int tid);

    @Select("select * from quiz where tid = #{tid}")
    @Results({
                    @Result(column = "qid", property = "qid"),
                    @Result(column = "qid", property = "options", many = @Many(select = "getOptionByQid"))
            })
    public List<Quiz> getQuizByTid(int tid);

    @Select("select * from `option` where qid  = #{qid}")
    public List<Option> getOptionByQid(int qid);
}
