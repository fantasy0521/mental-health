package com.mhs.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mhs.entity.Quiz;


public interface IQuizMapper extends BaseMapper<Quiz> {

}
