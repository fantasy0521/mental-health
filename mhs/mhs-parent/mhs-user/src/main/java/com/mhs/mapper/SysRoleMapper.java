package com.mhs.mapper;

import com.mhs.entity.SysRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author Fantasy0521
 * @since 2023-04-03
 */
public interface SysRoleMapper extends BaseMapper<SysRole> {

    List<String> selectRoleKeyByUserId(Long userId);


    void updateSysRoleUser(@Param("roleId") Long roleId,@Param("userId") Long userId);
}
