package com.mhs.vo;

import com.mhs.entity.User;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserVo extends User {
    
    //用户角色类型
    private String role;
    
    
}
