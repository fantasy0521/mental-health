package com.mhs.service;

import com.mhs.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;
import com.mhs.vo.Result;
import com.mhs.vo.UserVo;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author Fantasy0521
 * @since 2023-04-01
 */
public interface IUserService extends IService<User> {

    Result login(User user);

    Result logout();

    Result loginByWeixin(String code,String avatarUrl,String nickName);

    Result register(User user,Integer roleId);

    Result registerOrLogin(User user);

    Boolean isAdmin(User user);

    Result getUserList(String search, Integer pageNum, Integer pageSize);

    Result updateUser(UserVo userVo);
}
