package com.mhs.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@Data
@ConfigurationProperties(prefix = "mynacos")
public class MyNacosTestConfig {
    
    private String test;
    
}
