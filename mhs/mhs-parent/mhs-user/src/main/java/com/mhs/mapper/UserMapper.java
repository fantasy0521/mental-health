package com.mhs.mapper;

import com.mhs.entity.SysRole;
import com.mhs.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mhs.vo.UserVo;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author Fantasy0521
 * @since 2023-04-01
 */
public interface UserMapper extends BaseMapper<User> {

    @Insert("insert into user values(null,#{userName},#{nickName},#{password},'0',#{email},#{phonenumber},'0',null,0,now(),now(),0)")
    @Options(useGeneratedKeys = true,keyProperty="id")
    Long insertUser(User user);
    
    @Insert("insert into sys_user_role values(#{userId},#{roleId})")
    void insertUserRole(@Param("userId") Long userId,@Param("roleId") Integer roleId);
    
    SysRole getUserRoleByuserId(Long userId);

    List<UserVo> getUserList(@Param("search")String search, @Param("start")Integer start,@Param("pageSize") Integer pageSize);


}
