package com.mhs.service;

import com.mhs.entity.SysRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author Fantasy0521
 * @since 2023-04-03
 */
public interface ISysRoleService extends IService<SysRole> {

}
