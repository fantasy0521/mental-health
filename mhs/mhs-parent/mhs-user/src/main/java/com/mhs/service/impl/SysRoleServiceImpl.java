package com.mhs.service.impl;

import com.mhs.entity.SysRole;
import com.mhs.mapper.SysRoleMapper;
import com.mhs.service.ISysRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author Fantasy0521
 * @since 2023-04-03
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {

}
