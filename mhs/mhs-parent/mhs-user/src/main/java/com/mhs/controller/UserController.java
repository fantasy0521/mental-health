package com.mhs.controller;


import com.alibaba.fastjson.JSON;
import com.mhs.config.MyNacosTestConfig;
import com.mhs.entity.User;
import com.mhs.exception.BizException;
import com.mhs.service.IUserService;
import com.mhs.vo.Jscode2session;
import com.mhs.vo.Result;
import com.mhs.vo.UserVo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.List;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author Fantasy0521
 * @since 2023-04-01
 */
@RestController
@RequestMapping("/mhs/user")
public class UserController {

    @Resource
    private IUserService userService;

    @GetMapping("helloAdmin")
    //设置权限,必须有admin这个权限才能进入此方法
//    @PreAuthorize("hasAuthority('admin')")
    @PreAuthorize("@fse.hasAuthority('admin')")//调用自定义的权限认证方法
    public String helloAdmin() {
        return "hello Admin";
    }

    /**
     * 获取所有用户信息
     *
     * @return
     */
    @GetMapping("getUserList")
    @PreAuthorize("@fse.hasAuthority('admin')")
    public Result getUserList(@RequestParam(name = "content" ,required = false) String search,
                              @RequestParam(defaultValue = "1") Integer pageNum,
                              @RequestParam(defaultValue = "10") Integer pageSize) {
        return userService.getUserList(search,pageNum,pageSize);
    }

    /**
     * 修改用户信息
     * @return
     */
    @PostMapping("updateUser")
    @PreAuthorize("@fse.hasAuthority('admin')")
    public Result updateUser(@RequestBody UserVo userVo){
        return userService.updateUser(userVo);
    }
    
    


    @GetMapping("hello")
    public String hello() {
        return "hello";
    }

    //登陆
    @PostMapping("login")
    public Result login(@RequestBody User user) {
        return userService.login(user);
    }

    //管理员登陆
    @PostMapping("loginAdmin")
//    @PreAuthorize("@fse.hasAuthority('admin')")  未登陆无法进行security权限认证
    public Result loginAdmin(@RequestBody User user) {
        //进行权限认证,判断当前用户是否为管理员
        Boolean isAdmin = userService.isAdmin(user);
        if (!isAdmin){
            return Result.failure("您不是管理员");
        }
        //是管理员,进行登陆验证
        Result login = userService.login(user);
        return new Result(200,login.getMsg(),login.getData());
    }

    //登出
    @RequestMapping("logout")
    public Result logout() {
        return userService.logout();
    }

    //用户名密码注册或登陆
    @PostMapping("register")
    public Result register(@RequestBody User user) {
        return userService.registerOrLogin(user);
    }

    //注册管理员
    @PostMapping("registerAdmin")
    @PreAuthorize("@fse.hasAuthority('admin')")
    public Result registerAdmin(@RequestBody User user) {
        return userService.register(user, 3);
    }


    //微信登陆
    @GetMapping("loginByWeixin")
    public Result loginByWeixin(String code, String avatarUrl, String nickName) {
        return userService.loginByWeixin(code, avatarUrl, nickName);
    }

    //测试nacos配置管理
    @Resource
    private MyNacosTestConfig myNacosTestConfig;

    @GetMapping("nacos")
    public String nacos() {
        return myNacosTestConfig.getTest();
    }

    /**
     * 通过userId获取user对象
     *
     * @param userId
     * @return
     */
    @RequestMapping("getUserById")
    public User getUserById(@RequestParam Long userId) {
        return userService.getById(userId);
    }

}

