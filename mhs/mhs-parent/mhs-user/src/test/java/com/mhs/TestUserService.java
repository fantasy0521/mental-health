package com.mhs;

import com.mhs.entity.User;
import com.mhs.service.IUserService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

@SpringBootTest
public class TestUserService {
    
    @Resource
    private IUserService userService;
    
    @Test
    public void testIsAdmin(){
        User user = new User();
        user.setUserName("zhangsan");
        user.setPassword("a");
        System.out.println(userService.isAdmin(user));
    }
    
}
