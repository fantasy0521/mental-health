package com.mhs;

import com.mhs.mapper.SysRoleMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class TestSysRoleMapper {
    @Autowired
    private SysRoleMapper sysRoleMapper;

    @Test
    public void testSelectPermsByUserId(){
        List<String> list = sysRoleMapper.selectRoleKeyByUserId(2l);
        System.out.println(list);
    }


}
