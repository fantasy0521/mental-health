package com.mhs;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.mhs.entity.User;
import com.mhs.mapper.UserMapper;
import com.mhs.vo.UserVo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.List;

@SpringBootTest
public class TestUserMapper {
    
    @Autowired
    private UserMapper userMapper;
    
    @Test
    public void test(){
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getUserName,"zhangsan");
        User user = userMapper.selectOne(queryWrapper);
    }
    
    @Test
    public void testBC(){
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encode = passwordEncoder.encode("a");
        System.out.println(encode);
    }
    
    @Test
    public void testGetUserList(){
        List<UserVo> list = userMapper.getUserList("用户", 0, 10);
        System.out.println(list);
    }

}
