```
nohup java   -Xms64m -Xmx128m    -jar  xxx.xxxx-xxx-xxxxxx-0.0.1-SNAPSHOT.jar &
```

[Spring cloud开发的微服务部署到Linux上内存过高的问题 - 勇敢-的心 - 博客园 (cnblogs.com)](https://www.cnblogs.com/heyi-77/p/9025865.html)

```
java   -Xms32m -Xmx64m    -jar  mhs-gateway.jar
```

```
nohup java   -Xms32m -Xmx64m    -jar  mhs.jar &
```

在启动服务时进行jvm参数设置

![image-20230422120838563](assets/image-20230422120838563.png)

所有服务启动后:

![image-20230422121222044](assets/image-20230422121222044.png)

![image-20230422121250402](assets/image-20230422121250402.png)

设置虚拟内存

https://blog.csdn.net/choviwu/article/details/110187440

![image-20230427132001841](assets/image-20230427132001841.png)

![image-20230427132010816](assets/image-20230427132010816.png)